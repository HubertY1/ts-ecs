class Registry<T = object>{
    forward: { [s: string]: T };
    backward: Map<T, string>;
    register(key: string, value: T) {
        this.forward[key] = value;
        this.backward.set(value, key);
        return key;
    }
    get(key: string) {
        return this.forward[key];
    }
    getKey(obj: T) {
        return this.backward.get(obj);
    }
    constructor() {
        this.forward = {};
        this.backward = new Map();
    }
}

class NumericRegistry<T = object>{
    forward: T[];
    backward: Map<T, number>;
    register(value: T) {
        this.backward.set(value, this.forward.length);
        this.forward.push(value);
        return this.forward.length - 1;
    }
    get(key: number) {
        return this.forward[key];
    }
    getKey(obj: T) {
        return this.backward.get(obj);
    }
    constructor(data: T[] = []) {
        this.forward = data;
        this.backward = new Map();
        for (let i = 0; i < this.forward.length; i++) {
            this.backward.set(this.forward[i], i);
        }
    }
}


function deserialize(ref: number | string, globalRegistry: Registry<Function | object>, objectRegistry: NumericRegistry<object>) {
    if (typeof ref === "string") {
        let lookup = globalRegistry.get(ref);
        if (lookup !== undefined) {
            return lookup;
        }
        else {
            throw new ReferenceError(`global ref ${ref} not found!`);
        }
    }
    else if (typeof ref === "number") {
        let built = objectRegistry.forward.map((schema: any) => {
            let proto = null;
            if (schema.proto && typeof schema.proto === "object") {
                if (schema.proto.i !== undefined) {
                    let i = schema.proto.i;
                    if (typeof i === "string") {
                        let lookup = globalRegistry.get(i);
                        if (lookup !== undefined) {
                            proto = lookup;
                        }
                        else {
                            throw new ReferenceError(`global ref ${i} not found!`);
                        }
                    }
                    else if (typeof i === "number") {
                        throw new ReferenceError("unregisterd prototypes support not implemented yet!");
                    }
                    else {
                        throw new ReferenceError(`unrecognized ref ${i}!`);
                    }
                }
            }
            return Object.create(proto);
        });
        for (let i = 0; i < built.length; i++) {
            let schema = <any>objectRegistry.get(i);
            let obj = built[i];

            for (let prop of Object.keys(schema.data)) {
                let descriptor = { ...schema.data[prop] };
                if (descriptor.value && typeof descriptor.value === "object") {
                    if (descriptor.value.i !== undefined) {
                        let i = descriptor.value.i;
                        if (typeof i === "string") {
                            let lookup = globalRegistry.get(i);
                            if (lookup !== undefined) {
                                descriptor.value = lookup;
                            }
                            else {
                                throw new ReferenceError(`global ref ${i} not found!`);
                            }
                        }
                        else if (typeof i === "number") {
                            let lookup = built[i];
                            if (lookup !== undefined) {
                                descriptor.value = lookup;
                            }
                            else {
                                throw new ReferenceError(`local ref ${i} not found!`);
                            }
                        }
                        else {
                            throw new ReferenceError(`unrecognized ref ${i}!`);
                        }
                    }
                }
                Object.defineProperty(obj, prop, descriptor);
            }
        }
        return built[ref];
    }
}

type GlobalRef = { [s: string]: string };
type LocalRef = { [s: string]: number };

function serialize(item: any, globalRegistry: Registry<Function | object>, workingRegistry: NumericRegistry<object>, outputRegistry: NumericRegistry<object>, depthRemaining: number) {
    if (depthRemaining === 0) {
        throw RangeError("object is too deep");
    }
    else if (typeof item === "bigint" || typeof item === "symbol" || typeof item === "undefined") {
        throw new TypeError(`unsupported type ${typeof item}`);
    }
    else if (typeof item === "function") {
        const x = globalRegistry.getKey(item);
        if (x) {
            return { i: x };
        }
        throw new ReferenceError(`unregistered function ${item}`);
    }
    else if (item && typeof item === "object") {
        const x = globalRegistry.getKey(item);
        if (x) {
            return { i: x };
        }
        const y = workingRegistry.getKey(item);
        if (y) {
            return { i: y };
        }

        let serial = <any>null;
        const proto = Object.getPrototypeOf(item);
        if (proto) {
            serial = { proto: serialize(proto, globalRegistry, workingRegistry, outputRegistry, depthRemaining - 1), data: {} };
        }
        else {
            serial = { proto: null, data: {} };
        }
        let i = workingRegistry.register(item);
        outputRegistry.register(serial);
        for (let prop of Object.getOwnPropertyNames(item)) {
            const desc = <PropertyDescriptor>Object.getOwnPropertyDescriptor(item, prop);
            if (desc.get || desc.set) {
                throw new ReferenceError(`unregistered object ${item} has getter or setter defined for prop ${prop}`);
            }
            else {
                if (desc.value !== undefined) {
                    desc.value = serialize(desc.value, globalRegistry, workingRegistry, outputRegistry, depthRemaining - 1);
                    serial.data[prop] = desc; //Not a typo. We want the actual descriptor literal.
                }
                else {
                    throw new ReferenceError(`unregisted object ${item} has no value defined for prop ${prop}`);
                }
            }
        }
        return { i };
    }
    else {
        return item;
    }
}



class Serializer {
    MAX_DEPTH: number;
    globalRegistry: Registry<Function | Object>;
    customSerializers: Map<object, { serialize: (arg: object) => object, deserialize: (arg: object) => object }>;
    deserialize(s: string) {
        let data = JSON.parse(s);
        return deserialize(data[1].i, this.globalRegistry, new NumericRegistry(data[0]));
    }
    serialize(obj: any) {
        let out = new NumericRegistry();
        return JSON.stringify([out.forward, serialize(obj, this.globalRegistry, new NumericRegistry(), out, this.MAX_DEPTH)]);
    }
    register(s: string, obj: object | Function, custom?: { serialize: (arg: object) => object, deserialize: (arg: object) => object }) {
        if (!s || !s.length) {
            throw new TypeError("Must provide a name!");
        }
        if (s[0] === "_") {
            throw new TypeError(`Provided name ${s} cannot start with an underscore!`);
        }
        if (this.globalRegistry.getKey(obj)) {
            throw new TypeError(`the object ${obj} is already registered under key ${this.globalRegistry.getKey(obj)}!`);
        }
        if (this.globalRegistry.get(s)) {
            throw new TypeError(`name ${name} is taken!`);
        }
        else {
            this.globalRegistry.register(s, obj);
            if (custom) {
                this.customSerializers.set(obj, custom);
            }
        }
    }
    constructor(maxDepth: number = 20) {
        this.MAX_DEPTH = maxDepth;
        this.globalRegistry = new Registry();
        this.customSerializers = new Map();
        this.globalRegistry.register("_obj", Object.prototype);
        this.globalRegistry.register("_arr", Array.prototype);
    }
}

export { Serializer }