import * as ECS from "./index"
import * as Serial from "./serialize"

const ecs = new ECS.Manager();

class Vec2 {
    x: number;
    y: number;
    /**
     * Mutates the vector.
     */
    public add(v: Vec2) {
        this.x += v.x;
        this.y += v.y;
    }
    public lengthSquared(): number {
        return this.x * this.x + this.y * this.y;
    }
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}

const Physics = ecs.createComponent("Physics", {
    position: new Vec2(0, 0),
    velocity: new Vec2(0, 0),
    acceleration: new Vec2(0, 0)
});

const PhysicsPool = ecs.createPool()
    .require(Physics)
    .listen();

const PhysicsSystem = ecs.createSystem("PhysicsSystem", [PhysicsPool], (PhysicsEntities) => {
    PhysicsEntities.forEach((entity) => {
        entity.Physics.velocity.add(entity.Physics.acceleration);
        entity.Physics.position.add(entity.Physics.velocity);
    });
});

const x = new Vec2(0, 0);
const TestEntity = ecs.createEntity()
    ._mountRaw(Physics, {
        position: x,
        velocity: x,
        acceleration: new Vec2(1, 1),
    });

PhysicsSystem.update();

const serial = new Serial.Serializer();
serial.register("BaseEntity", ECS.BaseEntity.prototype);
serial.register("Vec2", Vec2.prototype);
let s = serial.serialize(TestEntity);
let o = serial.deserialize(s);

console.log(TestEntity);
console.log(s);
console.log(o);


