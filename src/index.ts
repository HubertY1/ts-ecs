type LegalComponentData = any;
type StringIndexed<T> = { [s: string]: T }


//the verbose type { [key in S]: D } instead of using KeyValue<S, D> makes TypeScript intellisense more verbose too, for some reason.
type _KeyValue<S extends string, D> = { [key in S]: D }
type LegalComponentSchema = _KeyValue<string, LegalComponentData>


function ObjectAssignRecursive(obj: any, props: PropertyDescriptorMap, depth = 0) {
    const MAX_DEPTH = 10;
    if (depth < MAX_DEPTH) {
        for (let key of Object.keys(props)) {
            if (typeof props[key] === "object") {
                if (typeof obj[key] === "object") {
                    ObjectAssignRecursive(obj[key], <PropertyDescriptorMap>props[key], depth + 1);
                }
                else {
                    throw TypeError(`type mismatch for key ${key}: ${props}.${key} is of type object but ${obj}.${key} is of type ${obj[key]}`);
                }
            }
            else {
                obj[key] = props[key];
            }
        }
    }
    else {
        throw RangeError("object is too deep!");
    }
}

/**
 * @template S - The name of the component as a string.
 * @template D - The data type of the component.
 */
export class Component<S extends string, D extends LegalComponentData> {
    name: S;
    schema: { [key in S]: D };
    defaults: D;
    defaultsString: string;
    defaultsFn?: () => D;
    mountListeners: Set<(entity: Entity<{ [key in S]: D }>) => void>;
    /**
      * 
      * @param callback - A callback to be executed when the component is mounted onto an entity.
      */
    public registerMountListener(callback: (entity: Entity<{ [key in S]: D }>) => void): void {
        this.mountListeners.add(callback);
    }

    /**
     * Generates component data. Deep copy of default values is guaranteed.
     * @param args - Component value data that will overwrite the component defaults.
     * @param deepcopy - Whether or not to deep copy provided data. Defaults to false.
     * @returns An object containing the component value data.
     */
    public initialize(args?: any, deepcopy: boolean = false): D {
        let defaults = this.defaultsFn ? this.defaultsFn() : JSON.parse(this.defaultsString);
        if (typeof defaults === "object") {
            let ret = defaults;
            ObjectAssignRecursive(ret, JSON.parse(this.defaultsString))
            if (args) {
                if (typeof args === "object") {
                    ObjectAssignRecursive(ret, <PropertyDescriptorMap>args);
                }
                else {
                    throw TypeError("wrong argument type: expected object");
                }
            }
            return ret;
        }
        else if (typeof args === typeof defaults) {
            return <D>args;
        }
        else if (!args) {
            return defaults;
        }
        else {
            throw TypeError("wrong argument type: expected " + typeof defaults);
        }
    }
    /**
     * An ECS component. Don't call this manually, call createComponent() from a Manager object instead.
     * @param schema - The description of the component, as a key-value pair of data that should be written to entities.
     */
    constructor(name: S, defaults: D, defaultsFn?: () => D) {
        this.name = name;
        this.defaults = defaults;
        this.defaultsString = JSON.stringify(defaults);
        this.defaultsFn = defaultsFn;
        this.schema = <{ [key in S]: D }>{ [name]: defaults };
        this.mountListeners = new Set();

        if (this.defaultsFn) {
            if (JSON.stringify(this.defaultsFn()) !== this.defaultsString) {
                throw TypeError("defaults function mismatch");
            }
        }
    }
}

type Entity<T> = BaseEntity & T;
type GenericEntity = Entity<LegalComponentSchema>;

class BaseEntity {
    public static mount<T, S extends string, D extends LegalComponentData>(entity: Entity<T>, component: Component<S, D>, args?: LegalComponentData): Entity<T & { [key in S]: D }> {
        const data = component.initialize(args);
        (<Entity<T & { [key in S]: D }>>entity)[component.name] = <any>data; //yikes
        for (let callback of component.mountListeners) {
            callback(<Entity<T & { [key in S]: D }>>entity);
        }
        return <Entity<T & { [key in S]: D }>>entity;
    }

    public static mountRaw<T, S extends string, D extends LegalComponentData>(entity: Entity<T>, component: Component<S, D>, args: D): Entity<T & { [key in S]: D }> {
        (<Entity<T & { [key in S]: D }>>entity)[component.name] = <any>args;
        return <Entity<T & { [key in S]: D }>>entity;
    }

    _id: number;

    public _mount<S extends string, D extends LegalComponentData>(comp: Component<S, D>, args?: LegalComponentData) {
        return BaseEntity.mount(this, comp, args)
    }

    /**
     * Literally just performs an property assignment. Does not call any listeners or perform any runtime type checking.
     * @param comp 
     * @param args 
     */
    public _mountRaw<S extends string, D extends LegalComponentData>(comp: Component<S, D>, args: D) {
        return BaseEntity.mountRaw(this, comp, args)
    }

    public get _alive(): boolean {
        return this._id !== -1;
    }

    constructor() {
        this._id = -1;
    }
}

class Pool<T> {
    components: Component<any, any>[];
    private _data: Set<Entity<T>>;
    _filter: (entity: Entity<any>) => boolean;
    /**
     * Checks if an entity has eligible components to be placed in the pool.
     * @param entity
     * @returns {boolean}
     */
    safe(entity: Entity<any>): boolean {
        return this.components.every(component => entity[component.name] !== undefined);
    }
    /**
     * Checks if the pool contains an entity.
     * @param entity
     * @returns {boolean}
     */
    has(entity: Entity<any>): boolean {
        return this._data.has(entity);
    }
    /**
     * Removes an entity from the pool.
     * @param entity - The entity to remove from the pool.
     * @returns {boolean} - Whether the entity was successfully removed from the pool.
     */
    delete(entity: Entity<any>): boolean {
        return this._data.delete(entity);
    }
    /**
     * Set a custom filtering function used by listen(). Defaults to safe().
     * @param fn A filtering function that returns true if the entity should be added to the pool.
     */
    filter(fn: (entity: Entity<any>) => boolean) {
        this._filter = fn;
    }
    /**
     * Adds listeners that fire when entities mount the specified components, adding them to the pool if they pass the filtering function specified by filter().
     * @param components The components to listen to. If no components are specified, defaults to all components required by the pool.
     */
    listen(...components: Component<string, any>[]): this {
        if (!components.length) {
            components = this.components;
        }
        for (let component of components) {
            component.registerMountListener((entity) => {
                if (!this.has(entity) && (!this._filter || this._filter(entity))) {
                    this.add(<Entity<T>>entity, true);
                }
            });
        }
        return this;
    }
    /**
     * Adds an entity to the pool.
     * @param entity - The entity to add to the pool.
     * @param force - Whether to skip checking component requirements. Defaults to false.
     * @returns The pool.
     */
    add(entity: Entity<T>, force: boolean = false): this {
        if (force || this.safe(entity)) {
            this._data.add(entity);
        }
        else {
            throw TypeError(`Entity ${entity} does not have the required components to be added to the pool`);
        }
        return this;
    }

    /**
     * Calls a callback for each entity in the pool.
     * @param callback - The function to be called for each entity.
     * @param thisArg - An argument to be used as the this context.
     */
    forEach(callback: (entity: Entity<T>) => void, thisArg?: any) {
        this._data.forEach(callback, thisArg);
    }

    require<S extends string, D extends LegalComponentData>(comp: Component<S, D>): Pool<T & typeof comp.schema> {
        this.components.push(comp);
        return <Pool<T & typeof comp.schema>><any>(this);
    }
    /**
     * Should not be called. Use createPool() from a Manager instead.
     */
    constructor() {
        this.components = [];
        this._data = new Set();
        this._filter = this.safe;
    }
}

class System<T extends Pool<any>[]> {
    name: string;
    pools: T;
    updateFn: (...pools: T) => any;
    /**
     * Calls the update function with the registered pools.
     */
    update() {
        this.updateFn(...this.pools);
    }
    /**
     * An ECS System that has access to entity pools.
     * @param name - The name of the system.
     * @param pools - Pools of objects to be fed into System.update().
     * @param updateFn - The update function to be called on the provided pools.
     */
    constructor(name: string, pools: T, updateFn: (...pools: T) => any) {
        this.name = name;
        this.pools = pools;
        this.updateFn = updateFn;
    }
}

const EMPTY_OBJECT = {};

class Manager {
    entities: (GenericEntity | null)[];
    empty: number[];
    components: StringIndexed<Component<any, any>>;
    systems: System<any>[];
    pools: Pool<any>[];
    //serialization: SerializationRegistry;

    destroyEntity(id: number) {
        if (this.entities[id]) {
            this.entities[id]!._id = -1;
            this.entities[id] = null;
            this.empty.push(id);
        }
    }

    createEntity(): BaseEntity {
        let id = this.empty.length ? <number>this.empty.pop() : this.entities.length;
        let ret = new BaseEntity();
        ret._id = id;
        this.entities[id] = ret;
        return ret;
    }
    /**
     * Create and register a new Component to the registry. Does not actually return the component itself, but rather its schema, for mounting and type inference purposes.
     * @param name - The component name. Two components cannot have the same name, and the name must not begin with an underscore.
     * @param defaults - The component defaults.
     * @returns - A schema object used as a lookup key in the component registry. To access the actual component, do Manager.components.get(key).
     */
    createComponent<S extends string, D extends LegalComponentData>(name: S, defaults: D, defaultsFn?: () => D) {
        if ((<any>EMPTY_OBJECT)[name]) {
            throw new TypeError(`component name '${name}' is a reserved keyword`);
        }
        if (name.length === 0) {
            throw new TypeError("component name must be of nonzero length");
        }
        if (name[0] === '_') {
            throw new TypeError("component name cannot begin with underscore");
        }
        if (this.components[name]) {
            throw new TypeError(`Already exists component with name '${name}'`);
        }
        let comp = new Component(name, defaults, defaultsFn);
        this.components[name] = comp;
        return comp;
    }

    createSystem<T extends Pool<any>[]>(name: string, pools: T, updateFn: (...pools: T) => any) {
        let ret = new System(name, pools, updateFn);
        this.systems.push(ret);
        return ret;
    }

    createPool(): Pool<{}> {
        let ret = new Pool();
        this.pools.push(ret);
        return <Pool<{}>>ret;
    }

    constructor() {
        this.entities = [];
        this.empty = [];
        this.components = {};
        this.pools = [];
        this.systems = [];
    }
}

export { Manager, BaseEntity };
