# typescript-ecs

entity component system framework in typescript. can also be used in javascript with typescript type inference.

Example:
```typescript
import * as ECS from "ts-ecs"

const ecs = new ECS.Manager();

class Vec2 {
    x: number;
    y: number;
    //mutates the vector.
    public add(v: Vec2) {
        this.x += v.x;
        this.y += v.y;
    }
    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}

//create a Component called Physics and specify its shape and default values
const Physics = ecs.createComponent("Physics", {
    position: new Vec2(0, 0),
    velocity: new Vec2(0, 0),
    acceleration: new Vec2(0, 0)
});

//create a Pool container that contains with any Entities with Physics
const PhysicsPool = ecs.createPool()
    .require(Physics)
    .listen();

//create a System that performs acts on any Entities in the PhysicsPool
const PhysicsSystem = ecs.createSystem("PhysicsSystem", [PhysicsPool], (PhysicsEntities) => {
    //these Entities are guaranteed to have Physics as PhysicsPool requires Physics
    PhysicsEntities.forEach((entity) => {
        entity.Physics.velocity.add(entity.Physics.acceleration);
        entity.Physics.position.add(entity.Physics.velocity);
    });
});

//create an Entity and mount Physics to it, automatically adding it to the PhysicsPool
const TestEntity = ecs.createEntity()
    ._mountRaw(Physics, {
        position: new Vec2(0, 0),
        velocity: new Vec2(1, 1),
        acceleration: new Vec2(1, 0),
    });

console.log(TestEntity.Physics.position); //Vec2(0, 0)

//call the PhysicsSystem, updating all Entities with Physics
PhysicsSystem.update();

console.log(TestEntity.Physics.position); //Vec2(2, 1)
```